package com.hzhihui.flutter_umeng_messaging;

import android.app.Application;
import android.util.Log;

import com.umeng.commonsdk.UMConfigure;
import com.umeng.message.IUmengRegisterCallback;
import com.umeng.message.PushAgent;

import org.android.agoo.huawei.HuaWeiRegister;
import org.android.agoo.mezu.MeizuRegister;
import org.android.agoo.oppo.OppoRegister;
import org.android.agoo.vivo.VivoRegister;
import org.android.agoo.xiaomi.MiPushRegistar;

public class UMApplication extends Application {
    private static final String TAG = UMApplication.class.getName();

    private PushAgent mPushAgent;

    @Override
    public void onCreate() {
        super.onCreate();

        String appkey = "5f682460b473963242a2ede0";
        String messageSecret = "43d9960d70d1a9c59aece5c88dff20a7";
        String channelName = "umeng";

        /**
         * Init
         */
        UMConfigure.init(this, appkey, channelName, UMConfigure.DEVICE_TYPE_PHONE, messageSecret);

        /**
         * Register
         */
        mPushAgent = PushAgent.getInstance(this);

        Log.i(TAG,"mPushAgent.register：-------->");
        mPushAgent.register(new IUmengRegisterCallback() {
            @Override
            public void onSuccess(String deviceToken) {
                Log.i(TAG,"Application注册成功：deviceToken：-------->  " + deviceToken);
//        channel.invokeMethod("onToken", deviceToken);
            }

            @Override
            public void onFailure(String s, String s1) {
                Log.e(TAG,"Application注册失败：-------->  " + "s:" + s + ",s1:" + s1);
            }
        });

        /**
         * 初始化厂商通道
         */
        //小米通道
        MiPushRegistar.register(this, "2882303761518686726", "5341868618726");
////        //华为通道，注意华为通道的初始化参数在minifest中配置
        HuaWeiRegister.register(this);
        Log.i(TAG,"HuaWeiRegister：deviceToken：-------->  ");


////        //魅族通道
//        MeizuRegister.register(this, "填写您在魅族后台APP对应的app id", "填写您在魅族后台APP对应的app key");
////        //OPPO通道
//        OppoRegister.register(this, "填写您在OPPO后台APP对应的app key", "填写您在魅族后台APP对应的app secret");
////        //VIVO 通道，注意VIVO通道的初始化参数在minifest中配置
//        VivoRegister.register(this);
    }
}
