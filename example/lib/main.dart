import 'dart:io';

import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:flutter_umeng_messaging/flutter_umeng_messaging.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';
  FlutterUmengMessaging messaging = FlutterUmengMessaging();

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.

    try {
      Map config;

      if (Platform.isIOS) {
        config = {"appkey":"5f66c09ba4ae0a7f7d08c908", "channel":"AppStore", "logEnabled": true, "encryptEnabled": false};
      }
      else if (Platform.isAndroid) {
        config = {"appkey":"5f682460b473963242a2ede0", "messageSecret": "43d9960d70d1a9c59aece5c88dff20a7", "channel":"umeng", "logEnabled": true, "encryptEnabled": false,
          "xiaomi": {
            "appId":"2882303761518686726",
            "appKey":"5341868618726",
          },
          "meizu": {
            "appId":"",
            "appKey":"",
          },
          "oppo": {
            "appId":"",
            "appSecret":"",
          },
          "vivo": {
          },
          "huawei": {
          },

        };
      }

      messaging.onTokenRefresh.listen((event) {
        print("Flutter-DeviceToken:"+event);
      });

      messaging.configure(
        onMessage: (Map<String, dynamic> message) async {
          print("Flutter-onMessage:"+message.toString());
        },
        onBackgroundMessage: (Map<String, dynamic> message) async {
          print("Flutter-onBackgroundMessage:"+message.toString());
        },
        onLaunch: (Map<String, dynamic> message) async {
          print("Flutter-onLaunch:"+message.toString());
        },
        onResume: (Map<String, dynamic> message) async {
          print("Flutter-onResume:"+message.toString());
        },
      );

      messaging.requestNotificationPermissions();
      await messaging.setup(config);

    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Text('Running on: $_platformVersion\n'),
        ),
      ),
    );
  }
}
